#!/bin/bash

# Prosty skrypt ułatwiający dodanie
# klienta do listy klientów. 

# 1 - list klientów
# 2 - adres ip klienta
# 3 - port klienta
# 4 - plik z kluczem publicznym klienta

if [ "$#" -ne 4 ]; then
	echo "Illegal number of parameters"
else

if [ -s $1 ]
then
	#Jest już plik z klientami
	echo -ne "\n" >> $1
fi

echo -ne $3 >> $1
echo -ne " " >> $1
echo -ne $2 >> $1
echo -ne " " >> $1
cat $4 >> $1

fi




